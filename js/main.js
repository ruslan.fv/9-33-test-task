$(document).ready(function () {
    var star = $('.content .star'), reset = $('.reset'), rateButton = $('.next-screen'), screen1 = $('.screen-1'),
        screen2 = $('.screen-2');
    var rating = 0;

    /**
     * Displaying the text under stars
     */
    star.hover(function () {
        var ratingText = '';
        switch ($(this).data('rating')) {
            case 1:
                ratingText = 'Very bad';
                break;
            case 2:
                ratingText = 'Bad';
                break;
            case 3:
                ratingText = 'Could be better';
                break;
            case 4:
                ratingText = 'Good';
                break;
            case 5:
                ratingText = 'Perfect!';
                break;
        }
        $('.value').text(ratingText);
    });

    /**
     * Set the rating
     */
    star.click(function () {
        rating = $(this).data('rating');
        star.each(function () {
            $(this).removeClass('rated');
            if ($(this).data('rating') <= rating) {
                $($(this).addClass('rated'));
            }
        });
    });

    /**
     * Move to the next screen if rated
     */
    rateButton.click(function () {
        if (rating > 0) {
            screen1.slideUp(200);
            screen2.slideDown(200);
            $('.rating-value').text(rating);
        }
    });

    /**
     * Moving back to rating
     */
    reset.click(function () {
        screen2.slideUp(200);
        screen1.slideDown(200);
    });
});